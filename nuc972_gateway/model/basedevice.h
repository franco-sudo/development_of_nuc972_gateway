#ifndef BASEDEVICE_H
#define BASEDEVICE_H
#include <QString>
class BaseDevice
{
public:
    explicit BaseDevice();
    QString get_name()const;
    int get_type()const;
    QString get_mac() const;
    int get_house()const;
    int get_maxtime()const;
    int get_countdown()const;

    void set_name(QString namestr);
    void set_type(int type);
    void set_mac(QString mac);
    void set_house(int houseid);
    void set_maxtime(int time);
    void set_countdown(int count);

    virtual void run(int cmd, const QString &gwmacs, QByteArray &arr)=0;
protected:
    QString name;
    int type;
    QString mac;
    int house;
    int maxtime;
    int countdown;

};

#endif // BASEDEVICE_H
