#ifndef CONDITIONTASK_H
#define CONDITIONTASK_H

#include <QObject>

class ConditionTask : public QObject
{
    Q_OBJECT
public:
    explicit ConditionTask(QObject *parent = nullptr);
    int get_id()const;
    QString get_name()const;
    QString get_cmd()const;
    QString get_scene()const;
    bool get_fortify()const;
    bool get_switch()const;

    void set_id(int id);
    void set_name(QString name);
    void set_cmd(QString cmd);
    void set_scene(QString scene);
    void set_fortity(bool fortity);
    void set_switchs(bool swith);
private:
    int id;
    QString name;
    QString cmd ;
    QString scene;
    bool fortify;
    bool switchs;

signals:

public slots:
};

#endif // CONDITIONTASK_H
