#ifndef DEVICETYPE_H
#define DEVICETYPE_H

#include <QString>

class DeviceType
{
public:
    explicit DeviceType(int id, QString tyname, QString tyimg="", QString tyunit="");
    int get_type_id() const;
    QString get_type_name()const;
    static QString get_type_name(int tyid);
    QString get_type_img() const;
    QString get_type_unit() const;
private:
    int type_id;
    QString type_name;
    QString type_img;
    QString type_unit;

};

#endif // DEVICETYPE_H
