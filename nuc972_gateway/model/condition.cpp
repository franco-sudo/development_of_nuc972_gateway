#include "condition.h"

Condition::Condition()
{

}
int Condition::get_task()const
{
    return this->task;
}
QString Condition::get_condition()const
{
    return this->condition;
}
bool Condition::get_state()const
{
    return this->state;
}
QString Condition::get_symbol()const
{
    return this->symbol;
}
double Condition::get_value()const
{
    return this->value;
}
void Condition::set_task(int task)
{
    this->task=task;
}
void Condition::set_condition(QString condition)
{
    this->condition=condition;
}
void Condition::set_state(bool state)
{
    this->state=state;
}
void Condition::set_symbol(QString symbol)
{
    this->symbol=symbol;
}
void Condition::set_value(double value)
{
    this->value=value;
}

