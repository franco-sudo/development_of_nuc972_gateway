#include "houseinfo.h"

HouseInfo::HouseInfo(QString id, QString name, QString floor, QString imgpath)
{
    this->id=id;
    this->name=name;
    this->img=imgpath;
}
QString HouseInfo::get_id()const
{
    return id;
}
QString HouseInfo::get_name()const
{
    return this->name;
}
QString HouseInfo::get_img() const
{
    return this->img;
}
QString HouseInfo::get_floor()const
{
    return this->floor;
}
