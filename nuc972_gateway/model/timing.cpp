#include "timing.h"

Timing::Timing()
{

}
QString Timing::get_task()const
{
    return this->task;
}
int Timing::get_week()const
{
    return this->week;
}
bool Timing::get_flag() const
{
    return this->flag;
}
void Timing::set_task(QString task)
{
    this->task=task;
}
void Timing::set_week(int week)
{
    this->week=week;
}
void Timing::set_flag(bool flag)
{
    this->flag=flag;
}
