#include "taskinfo.h"

TaskInfo::TaskInfo()
{

}
int TaskInfo::get_task_id()const
{
    return this->task_id;
}
QString TaskInfo::get_task_name()const
{
    return this->task_name;
}
int TaskInfo::get_type()const
{
    return this->task_type_id;
}
int TaskInfo::get_scene()const
{
    return this->task_scene;
}
QString TaskInfo::get_run_time()const
{
    return this->runtime;
}
QDateTime TaskInfo::get_create_time()const
{
    return this->create_time;
}
bool TaskInfo::get_state()const
{
    return this->state;
}
void TaskInfo::set_name(QString name)
{
    this->task_name=name;
}
void TaskInfo::set_type(int type)
{
    this->task_type_id=type;
}
void TaskInfo::set_scene(int scene)
{
    this->task_scene=scene;
}
void TaskInfo::set_statu(bool state)
{
    this->state=state;
}
void TaskInfo::set_run_time(QString rtime)
{
    this->runtime=rtime;
}
void TaskInfo::set_create_time(QDateTime ctime)
{
    this->create_time=ctime;
}



