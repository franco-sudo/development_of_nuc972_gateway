#ifndef MESSAGE_H
#define MESSAGE_H
#include <QByteArray>
#include <QTime>
class Message
{
public:
    explicit Message(int index,QByteArray arr);
    int get_index()const;
    QByteArray get_msg()const;
    void start();
    int get_elapsed();
    int get_num()const;
private:
    int index;
    QByteArray msg;
    QTime time;
    int number;
};

#endif // MESSAGE_H
