#ifndef SCENETIMING_H
#define SCENETIMING_H

#include <QString>
#include <QTime>
class SceneTiming
{
public:
    explicit SceneTiming();
    QString get_name()const;
    int get_scene()const;
    QTime get_time()const;
    bool get_state()const;
    void set_name(QString name);
    void set_scene(int scene);
    void set_time(QTime time);
    void set_state(bool state);
private:
    QString name;
    int scene;
    QTime time;
    bool state;
};

#endif // SCENETIMING_H
