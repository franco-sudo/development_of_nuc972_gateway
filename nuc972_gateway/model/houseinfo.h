#ifndef HOUSEINFO_H
#define HOUSEINFO_H

#include <QString>

class HouseInfo
{

public:
    explicit HouseInfo(QString id,QString name,QString floor,QString imgpath="");
    QString get_id()const;
    QString get_name() const;
    QString get_floor()const;
    QString get_img()const;

private:
    QString id;
    QString name;
    QString floor;
    QString img;

};

#endif // HOUSEINFO_H
