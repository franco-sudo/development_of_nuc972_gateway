#include "message.h"

Message::Message(int index,QByteArray arr):index(index),msg(arr)
{
    number=0;
}
int Message::get_index()const
{
    return this->index;
}
QByteArray Message::get_msg()const
{
    return msg;
}
void Message::start()
{
    number++;
    return time.start();
}
int Message::get_elapsed()
{
    return time.elapsed();
}
int Message::get_num() const
{
    return this->number;
}
