#ifndef TASKINFO_H
#define TASKINFO_H


#include <QDateTime>
class TaskInfo
{

public:
    explicit TaskInfo();
    int get_task_id()const;
    QString get_task_name()const;
    int get_type()const;
    int get_scene()const;
    QString get_run_time()const;
    QDateTime get_create_time()const;
    bool get_state()const;
    void set_name(QString name);
    void set_type(int type);
    void set_scene(int scene);
    void set_statu(bool state);
    void set_run_time(QString rtime);
    void set_create_time(QDateTime ctime);

private:
    int task_id;
    QString task_name;
    int task_type_id;
    QString runtime;
    QDateTime create_time;
    int task_scene;
    bool state;

};

#endif // TASKINFO_H
