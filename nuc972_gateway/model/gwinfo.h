#ifndef GWINFO_H
#define GWINFO_H

#include <QString>

class GwInfo
{
public:
    explicit GwInfo();
    QString get_mqtt_ip()const;
    int get_mqtt_port()const;
    QString get_num()const;
    QString get_mac()const;
    QString get_hub_topic()const;
    QString get_sub_topic()const;
    QString get_username()const;
    QString get_passwd()const;
    QString get_client_id()const;
    void set_mqtt_ip(QString ip);
    void set_mqtt_port(int port);
    void set_num(QString num);
    void set_mac(QString mac);
    void set_hub_topic(QString hub_topic);
    void set_sub_topic(QString sub_topic);
    void set_username(QString name);
    void set_passwd(QString passwd);
    void set_client_id(QString client_id);
private:
    QString mqttip;
    int mqttport;
    QString num;
    QString mac;
    QString hub_topic;
    QString sub_topic;
    QString username;
    QString passwd;
    QString client_id;
};

#endif // GWINFO_H
