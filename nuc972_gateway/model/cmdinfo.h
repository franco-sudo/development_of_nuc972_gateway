#ifndef INFRAREDCMDINFO_H
#define INFRAREDCMDINFO_H

#include <QString>

class CmdInfo
{
public:
    explicit CmdInfo(int id, QString name, QString devid, QString cmddata, QString tras, QString baud);
    int get_id()const;
    QString get_name() const;
    QString get_device_id()const;
    QString get_date() const;
    QString get_trans()const;
    QString get_baud()const;
    QString get_ctime()const;
private:
    int cmd_id;
    QString cmd_name;
    QString tras_type;
    QString baud;
    QString cmd_device;
    QString cmd_data;
    QString create_time;

};

#endif // INFRAREDCMDINFO_H
