#include "basedevice.h"

BaseDevice::BaseDevice()
{

}
QString BaseDevice::get_name()const
{
    return this->name;
}
int BaseDevice::get_type() const
{
    return this->type;
}

int BaseDevice::get_house()const
{
    return this->house;
}

QString BaseDevice::get_mac()const
{
    return this->mac;
}
int BaseDevice::get_maxtime()const
{
    return this->maxtime;
}
int BaseDevice::get_countdown()const
{
    return this->countdown;
}
void BaseDevice::set_name(QString namestr)
{
    this->name=namestr;
}
void BaseDevice::set_mac(QString mac)
{
    this->mac=mac;
}
void BaseDevice::set_type(int type)
{
    this->type=type;
}
void BaseDevice::set_house(int houseid)
{
    this->house=houseid;
}
void BaseDevice::set_maxtime(int time)
{
    this->maxtime=time;
}
void BaseDevice::set_countdown(int count)
{
    this->countdown=count;
}


