#include "devicetype.h"
#include <QPixmap>
DeviceType::DeviceType(int id, QString tyname, QString tyimg, QString tyunit)
{
    this->type_id=id;
    this->type_name=tyname;
    this->type_img=tyimg;
    this->type_unit=tyunit;
}
int DeviceType::get_type_id()const
{
    return this->type_id;
}
QString DeviceType::get_type_name()const
{
    return this->type_name;
}


QString DeviceType::get_type_img()const
{
    return this->type_img;
}
QString DeviceType::get_type_unit()const
{
    return this->type_unit;
}
