#include "sceneinfo.h"

SceneInfo::SceneInfo(QObject *parent) : QObject(parent)
{
}
SceneInfo::~SceneInfo()
{

}
void SceneInfo::set_id(int id)
{
    this->scene_id=id;
}
void SceneInfo::set_name(QString name)
{
    this->scene_name=name;
}
int SceneInfo::get_id()const
{
    return this->scene_id;
}
QString SceneInfo::get_name()const
{
    return this->scene_name;
}
void SceneInfo::run()
{

}
bool SceneInfo::is_run()
{
    return true;
}
