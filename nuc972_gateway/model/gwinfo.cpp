#include "gwinfo.h"

GwInfo::GwInfo()
{

}
QString GwInfo::get_mqtt_ip()const
{
    return this->mqttip;
}
int GwInfo::get_mqtt_port()const
{
    return this->mqttport;
}
QString GwInfo::get_mac()const
{
    return this->mac;
}
QString GwInfo::get_num()const
{
    return this->num;
}
QString GwInfo::get_hub_topic()const
{
    return this->hub_topic;
}
QString GwInfo::get_sub_topic()const
{
    return this->sub_topic;
}
QString GwInfo::get_username()const
{
    return this->username;
}
QString GwInfo::get_passwd()const
{
    return this->passwd;
}
QString GwInfo::get_client_id()const
{
    return this->client_id;
}
void GwInfo::set_mqtt_ip(QString ip)
{
     this->mqttip=ip;
}
void GwInfo::set_mqtt_port(int port)
{
    this->mqttport=port;
}
void GwInfo::set_mac(QString mac)
{
    this->mac=mac;
}
void GwInfo::set_num(QString num)
{
    this->num=num;
}
void GwInfo::set_hub_topic(QString hub_topic)
{
    this->hub_topic=hub_topic;
}
void GwInfo::set_sub_topic(QString sub_topic)
{
    this->sub_topic=sub_topic;
}
void GwInfo::set_username(QString name)
{
    this->username=name;
}
void GwInfo::set_passwd(QString passwd)
{
    this->passwd=passwd;
}
void GwInfo::set_client_id(QString client_id)
{
    this->client_id=client_id;
}
