#ifndef CONTROLLER_H
#define CONTROLLER_H


#include "basedevice.h"
class Controller : public BaseDevice
{
public:
    explicit Controller();
    void run(int cmd, const QString &gwmacs, QByteArray &arr);
};

#endif // CONTROLLER_H
