#ifndef SCENEINFO_H
#define SCENEINFO_H
#include <QObject>
#include <QString>
#include <QMap>
#include <QTimer>
class SceneInfo: public QObject
{
Q_OBJECT
public:
    explicit SceneInfo(QObject *parent=0);
    ~SceneInfo();
    void set_id(int id);
    void set_name(QString name);
    int get_id()const;
    QString get_name() const;
    void run();
    bool is_run();
private:
    int scene_id;
    QString scene_name;

};

#endif // SCENEINFO_H
