#ifndef CONDITION_H
#define CONDITION_H
#include <QString>

class Condition
{

public:
    explicit Condition();
    int get_task() const;
    QString get_condition()const;
    bool get_state()const;
    QString get_symbol()const;
    double get_value()const;

    void set_task(int task);
    void set_condition(QString condition);
    void set_state(bool state);
    void set_symbol(QString symbol);
    void set_value(double value);
private:

    int task;
    QString condition;
    bool state;
    QString symbol;
    double value;
};

#endif // CONDITION_H
