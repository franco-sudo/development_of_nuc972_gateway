#ifndef DEVICETIMING_H
#define DEVICETIMING_H

#include <QString>
#include <QTime>

class DeviceTiming
{

public:
    explicit DeviceTiming();
    QString get_name()const;
    int get_device()const;
    int get_cmd()const;
    QTime get_time()const;
    bool get_state()const;
    void set_name(QString name);
    void set_device(int dev);
    void set_cmd(int cmd);
    void set_time(QTime time);
    void set_state(bool state);
private:
    QString name;
    int device ;
    int cmd;
    QTime time;
    bool state;

};

#endif // DEVICETIMING_H
