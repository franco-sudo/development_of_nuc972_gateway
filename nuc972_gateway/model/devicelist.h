#ifndef DEVICELIST_H
#define DEVICELIST_H

class DeviceList
{
public:
    explicit DeviceList();
    void set_id(int id);
    void set_scene(int scene);
    void set_device(int device);
    void set_cmd(int cmd);
    void set_delay(int delay);
    int get_id()const;
    int get_scene()const;
    int get_device()const;
    int get_cmd()const;
    int get_delay()const;
private:
    int id;
    int scene;
    int device;
    int cmd;
    int delay;

};

#endif // DEVICELIST_H
