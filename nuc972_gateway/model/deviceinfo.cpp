#include "deviceinfo.h"
#include "QDateTime"
#include <QDebug>
DeviceInfo::DeviceInfo() :BaseDevice()
{
    this->id=0;
    this->name="";
    this->mac="";
    this->type=0;
    this->house=0;
    this->channel=0;
    this->maxtime=0;
    this->countdown=0;
}
int DeviceInfo::get_id()const
{
    return this->id;
}
int DeviceInfo::get_channel()const
{
    return this->channel;
}
void DeviceInfo::set_channel(int channel)
{
    this->channel=channel;
}
void DeviceInfo::set_id(int id)
{
    this->id=id;
}
void DeviceInfo::run(int cmd,const QString &gwmacs, QByteArray &arr)
{
    QStringList maclist=this->mac.split(' ',QString::SkipEmptyParts);
    for(QString mac:maclist)
    {
        bool b=false;
        int data= mac.toInt(&b,16);
        if(!b)
        {
            qDebug()<<"控制指令解析错误！错误数据："+data;
            return ;
        }
        arr.append((uchar)data);
    }
    arr.append(5);
    arr.append(8);
    QStringList gwmac=gwmacs.split(' ');
    for(QString i:gwmac)
    {
        bool b=false;
        int dat= i.toInt(&b,16);
        if(!b)
        {
            qDebug()<<"控制指令解析错误！错误数据："+dat;
            return ;
        }
        arr.append((uchar)dat);
    }
    arr.append(this->channel);
    arr.append(cmd);
}
