#include "cmdinfo.h"
#include <QDateTime>
CmdInfo::CmdInfo(int id, QString name, QString devid, QString cmddata, QString tras, QString baud)
{
    this->cmd_id=id;
    this->cmd_name=name;
    this->cmd_device=devid;
    this->cmd_data=cmddata;
    this->tras_type=tras;
    this->baud=baud;
    this->create_time=QDateTime::currentDateTime().toString("YYYY-MM-DD HH:MM:SS");
}
int CmdInfo::get_id() const
{
    return this->cmd_id;
}
QString CmdInfo::get_name()const
{
    return this->cmd_name;
}
QString CmdInfo::get_device_id()const
{
    return this->cmd_device;
}
QString CmdInfo::get_date()const
{
    return this->cmd_data;
}
QString CmdInfo::get_trans()const
{
    return this->tras_type;
}
QString CmdInfo::get_baud()const
{
    return this->baud;
}
QString CmdInfo::get_ctime()const
{
    return this->create_time;
}
