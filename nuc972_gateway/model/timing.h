#ifndef TIMING_H
#define TIMING_H
#include <QString>
class Timing
{
public:
    explicit Timing();
    QString get_task() const;
    int get_week()const;
    bool get_flag()const;
    void set_task(QString task);
    void set_week(int week);
    void set_flag(bool flag);
private:
    QString task;
    int week;
    bool flag;

};

#endif // TIMING_H
