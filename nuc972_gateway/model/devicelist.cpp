#include "devicelist.h"

DeviceList::DeviceList()
{

}
void DeviceList::set_id(int id)
{
    this->id=id;
}
void DeviceList::set_scene(int scene)
{
    this->scene=scene;
}
void DeviceList::set_device(int device)
{
    this->device=device;
}
void DeviceList::set_cmd(int cmd)
{
    this->cmd=cmd;
}
void DeviceList::set_delay(int delay)
{
    this->delay=delay;
}
int DeviceList::get_id()const
{
    return this->id;
}
int DeviceList::get_scene()const
{
    return this->scene;
}
int DeviceList::get_device()const
{
    return this->device;
}
int DeviceList::get_cmd()const
{
    return this->cmd;
}
int DeviceList::get_delay()const
{
    return this->delay;
}
