#include "scenetiming.h"

SceneTiming::SceneTiming()
{

}
QString SceneTiming::get_name()const
{
    return this->name;
}
int SceneTiming::get_scene()const
{
    return this->scene;
}
QTime SceneTiming::get_time()const
{
    return this->time;
}
bool SceneTiming::get_state()const
{
    return this->state;
}

void SceneTiming::set_name(QString name)
{
    this->name=name;
}
void SceneTiming::set_scene(int scene)
{
    this->scene=scene;
}

void SceneTiming::set_time(QTime time)
{
    this->time=time;
}
void SceneTiming::set_state(bool state)
{
    this->state=state;
}
