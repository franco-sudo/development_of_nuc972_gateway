#include "conditiontask.h"

ConditionTask::ConditionTask(QObject *parent) : QObject(parent)
{

}
int ConditionTask::get_id()const
{
    return this->id;
}
QString ConditionTask::get_name()const
{
    return this->name;
}
QString ConditionTask::get_cmd()const
{
    return this->cmd;
}
QString ConditionTask::get_scene()const
{
    return this->scene;
}
bool ConditionTask::get_fortify()const
{
    return this->fortify;
}
bool ConditionTask::get_switch()const
{
    return this->switchs;
}
void ConditionTask::set_id(int id)
{
    this->id=id;
}
void ConditionTask::set_name(QString name)
{
    this->name=name;
}
void ConditionTask::set_cmd(QString cmd)
{
    this->cmd=cmd;
}
void ConditionTask::set_scene(QString scene)
{
    this->scene=scene;
}
void ConditionTask::set_fortity(bool fortity)
{
    this->fortify=fortity;
}
void ConditionTask::set_switchs(bool swith)
{
    this->switchs=swith;
}
