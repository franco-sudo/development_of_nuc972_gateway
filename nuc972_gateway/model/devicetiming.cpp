#include "devicetiming.h"

DeviceTiming::DeviceTiming()
{

}

QString DeviceTiming::get_name()const
{
    return this->name;
}
int DeviceTiming::get_cmd()const
{
    return this->cmd;
}
int DeviceTiming::get_device()const
{
    return this->device;
}
QTime DeviceTiming::get_time()const
{
    return this->time;
}
bool DeviceTiming::get_state()const
{
    return this->state;
}

void DeviceTiming::set_name(QString name)
{
    this->name=name;
}
void DeviceTiming::set_cmd(int cmd)
{
    this->cmd=cmd;
}
void DeviceTiming::set_device(int dev)
{
    this->device=dev;
}
void DeviceTiming::set_time(QTime time)
{
    this->time=time;
}
void DeviceTiming::set_state(bool state)
{
    this->state=state;
}

