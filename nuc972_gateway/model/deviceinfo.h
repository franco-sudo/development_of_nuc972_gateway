#ifndef DEVICEINFO_H
#define DEVICEINFO_H

#include <QDateTime>
#include "basedevice.h"

class DeviceInfo : public BaseDevice
{
public:
    explicit DeviceInfo();
    int get_id()const;
    int get_channel()const;
    void set_channel(int channel);
    void set_id(int id);
    void run(int cmd, const QString &gwmacs, QByteArray &arr);
private:
    int id;
    int channel;
};

#endif // DEVICEINFO_H
