#include "mainwindow.h"
#include <QApplication>
#include "frminput.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    frmInput::Instance()->Init("control", "silvery", 12, 12);
//在ARM平台下运行，不显示鼠标箭头；全屏显示主窗口。
#ifdef __arm__
    QApplication::setOverrideCursor(Qt::BlankCursor);
    w.showMaximized();
#else
    w.show();
#endif
    return a.exec();
}
