#ifndef SERIALDATA_H
#define SERIALDATA_H

#include <QObject>
#include <QMutex>
#include <QSerialPort>
#include <QQueue>
class QSerialPort;
class QTimer;
class Message;
class GwInfo;
class SerialData : public QObject
{
    Q_OBJECT
public:
    explicit SerialData(GwInfo *info, QObject *parent = nullptr);
    bool reOpen(QString name,QSerialPort::BaudRate baud);
    void close();
private:
    QSerialPort *port;
    QTimer *tik;
    QQueue<Message*> *msg_queue;
    QList<Message*> *msg_list;
    QMutex lock;
    int index;
    //2秒重发
    int DELAY=2000;
    //最多重发次数
    int RESEND=3;
    void write_event(QByteArray array);
signals:
    void sendData(QString json);
private slots:
    void readData();
    void time_out();
public slots:
    void writeData(QByteArray array);
};

#endif // SERIALDATA_H
