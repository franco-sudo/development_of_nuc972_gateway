#ifndef MODBUSCLIENT_H
#define MODBUSCLIENT_H

#include <QObject>
class QModbusClient;
class ModbusClient : public QObject
{
    Q_OBJECT
public:
    explicit ModbusClient(QObject *parent = nullptr);
    bool start();
    bool isconnect();
private:
    QModbusClient *modbus;
    bool state;
signals:
    void senddata();
public slots:
    void sendaccess();
    void readread();
};
#endif // MODBUSCLIENT_H
