--
-- 文本编码：UTF-8
-- 数据库名：smarthome
--

-- 设备类型表：DEVICETYPE
CREATE TABLE DEVICETYPE
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --设备类型编号
    TYPENAME   VARCHAR (100)                            NOT NULL, --设备类型名称
    UNIT       VARCHAR (20)                                 NULL, --单位
    CREATETIME DATETIME                                 NOT NULL
);

-- 房间表：HOUSE
CREATE TABLE HOUSE 
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --房间编号
    HOUSENAME  VARCHAR (100)                            NOT NULL, --房间名称
    CREATETIME DATETIME                                 NOT NULL
);

-- 设备表：DEVICE
CREATE TABLE DEVICE
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --设备编号
    DEVICENAME VARCHAR (100)                            NOT NULL, --设备名称
    TYPE       INTEGER       REFERENCES DEVICETYPE (ID) NOT NULL, --设备类型
    HOUSE      INTEGER       REFERENCES HOUSE (ID)      NOT NULL, --设备房间
    CREATETIME DATETIME                                 NOT NULL
);

-- 指令表：CMD
CREATE TABLE CMD 
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --指令编号
    CMDNAME    VARCHAR (100)                            NOT NULL, --指令名称
    DEVICE     INTEGER       REFERENCES DEVICE (ID)     NOT NULL, --指令设备
    DATA       VARCHAR (200)                            NOT NULL, --指令内容
    CREATETIME DATETIME                                 NOT NULL
);

-- 情景模式表：SCENE
CREATE TABLE SCENE 
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --情景编号
    NAME       VARCHAR (100)                            NOT NULL  --情景模式
);
-- 情景指令表：SCENECMD
CREATE TABLE SCENECMD
(
    SCENE_ID   INTEGER      REFERENCES SCENE (ID)       NOT NULL, --情景编号
    CMD_ID     INTEGER      REFERENCES CMD (ID)         NOT NULL, --情景指令
    CREATETIME DATETIME                                 NOT NULL,
    PRIMARY KEY(SCENE_ID,CMD_ID)
);
-- 任务类型表：TASKTYPE
CREATE TABLE TASKTYPE 
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --任务类型
    TYPENAME   VARCHAR (100)                            NOT NULL, --类型名称
    CREATETIME DATETIME                                 NOT NULL
);

-- 任务表：TASK
CREATE TABLE TASK 
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --任务编号
    TASKNAME   VARCHAR (100)                            NOT NULL, --任务名称
    TYPE       INTEGER       REFERENCES TASKTYPE (ID)   NOT NULL, --任务类型
    RUNTIME    DATETIME                                 NOT NULL, --运行时间
    CREATETIME DATETIME                                 NOT NULL, --创建时间
    SCENE      INTEGER       REFERENCES SCENE (NAME)    NOT NULL, --运行情景模式
    STATE      BOOLEAN                                  NOT NULL  --状态
);
--系统配置表：SYSCONFIG
CREATE TABLE SYSCONFIG
(
    ID         INTEGER       PRIMARY KEY                NOT NULL, --编号
    MQTTIP     VARCHAR (50)                             NOT NULL, --MQTT服务器IP
    MQTTPORT   INTEGER                                  NOT NULL  --MQTT服务器端口
);


