#ifndef USERCLIENT_H
#define USERCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QByteArray>

class DataQuery;
class UserClient : public QObject
{
    Q_OBJECT
public:
    explicit UserClient(qintptr descrip, QObject *parent = nullptr);
    ~UserClient();
    qintptr getdescriptor() const;
    void start();
private:
    DataQuery *query;
    QTcpSocket *client;
    qintptr descriptor;
    QString clientname;
    void init_cmd();
    void analisys(QByteArray arr );
    QMap<QString,int> cmd;

signals:
    void sendtoserver(qintptr des, QByteArray arr);
    void error(QTcpSocket::SocketError);
    void offline(qintptr desc);
public slots:
    void readyread();
    void writedata(QByteArray data);
    void disconsoket();
};

#endif // USERCLIENT_H
