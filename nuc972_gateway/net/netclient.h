#ifndef NETCLIENT_H
#define NETCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>


class NetClient : public QObject
{
    Q_OBJECT
public:
    explicit NetClient(QString ip, int pot, QObject *parent = 0);             //
    void setipport(QString ip,int port);                                      //设置IP地址和端口
    void starting();                                                          //开始连接网络
    void setreconnecttime(int timespan);                                      //设置重连间隔
    void close();
private:
    QTcpSocket *cli;                                                          //Socket对象
    QString ipaddr;                                                           //IP地址
    int port;                                                                 //端口号
    int RECONNECTTIMES;                                                       //重连间隔
    bool isreconnect;
signals:
    void senddata(QByteArray arr);                                            //将读到的数据发送给绑定该信号的槽
public slots:
    void newconnect();                                                        //连接网络，如果连接失败，则5秒后重试
    void isconneced();                                                        //
    void readevent();                                                         //当有数据读取时执行
    void dropped();                                                           //断线时执行
    void error(QAbstractSocket::SocketError);                                 //网络有错误时执行
    void writevent(const QByteArray dataarr);                                 //发送数据
};

#endif // NETCLIENT_H
