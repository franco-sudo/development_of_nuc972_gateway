#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include <QObject>
class QUdpSocket;
class UdpClient : public QObject
{
    Q_OBJECT
public:
    explicit UdpClient(int port,QObject *parent = nullptr);
    void start();
private:
    QUdpSocket *udp;
signals:

public slots:
    void readyread();
    void sendevent(QString ip, int port, QByteArray arr);
};

#endif // UDPCLIENT_H
