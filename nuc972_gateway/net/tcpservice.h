#ifndef TCPSERVICE_H
#define TCPSERVICE_H

#include <QTcpServer>
#include <QMutex>
#include "userclient.h"
class TcpService : public QTcpServer
{
    Q_OBJECT
public:
    explicit TcpService(QObject *parent = 0);
    void closeall();
private:
    QMap<qintptr,UserClient*> *listclient;
    void incomingConnection(qintptr socketDescriptor);
    QMutex locker;
signals:
    void sendclientlist(QStringList*);
    void receiveclient(qintptr des,QByteArray buff);
    void sendtoall(QByteArray arr);
public slots:
    void deleteclient(qintptr socketDes);
    void sendtoclient(qintptr des, QByteArray arr);

};

#endif // TCPSERVICE_H
