#include "udpclient.h"
#include <QUdpSocket>
UdpClient::UdpClient(int port, QObject *parent) : QObject(parent)
{
    udp=new QUdpSocket;
    connect(udp,&QUdpSocket::readyRead,this,&UdpClient::readyread);
    udp->bind(port);

}
void UdpClient::readyread()
{
    qint64 len=udp->pendingDatagramSize();
    QHostAddress *remote;
    quint16 *port;
    QByteArray arr;
    qint64 lenght= udp->readDatagram(arr.data(),len,remote,port);

    qDebug(arr);
}
void UdpClient::sendevent(QString ip,int port,QByteArray arr)
{
    QHostAddress host(ip);
    udp->writeDatagram(arr,host,port);
}
