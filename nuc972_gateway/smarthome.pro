#-------------------------------------------------
#
# Project created by QtCreator 2018-11-10T21:17:39
#
#-------------------------------------------------

QT       += core gui network sql serialport serialbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = smarthome
TEMPLATE = app
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS \
    CONFIG_CTRL_IFACE
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    qtgpio.cpp \
    mqttclient.cpp \
    mainwindow.cpp \
    main.cpp \
    database/dataquery.cpp \
    mqtt/qmqtt_client_p.cpp \
    mqtt/qmqtt_client.cpp \
    mqtt/qmqtt_frame.cpp \
    mqtt/qmqtt_message.cpp \
    mqtt/qmqtt_network.cpp \
    mqtt/qmqtt_router.cpp \
    mqtt/qmqtt_routesubscription.cpp \
    mqtt/qmqtt_socket.cpp \
    mqtt/qmqtt_ssl_socket.cpp \
    mqtt/qmqtt_timer.cpp \
    mqtt/qmqtt_websocket.cpp \
    mqtt/qmqtt_websocketiodevice.cpp \
    net/netclient.cpp \
    net/tcpservice.cpp \
    net/udpclient.cpp \
    net/userclient.cpp \
    serialport/serialdata.cpp \
    userdatarequest.cpp \
    stringquery.cpp \
    signalbar.cpp \
    scanresultsitem.cpp \
    scanresults.cpp \
    peers.cpp \
    networkconfig.cpp \
    eventhistory.cpp \
    addinterface.cpp \
    ./src/common/wpa_ctrl.c \
    ./src/utils/os_unix.c \
    gwwpagui.cpp \
    frminput.cpp \
    model/gwinfo.cpp \
    model/deviceinfo.cpp \
    model/devicetype.cpp \
    model/houseinfo.cpp \
    model/sceneinfo.cpp \
    model/basedevice.cpp \
    model/cmdinfo.cpp \
    serialport/modbusclient.cpp \
    model/taskinfo.cpp \
    model/condition.cpp \
    model/conditiontask.cpp \
    model/timing.cpp \
    model/controller.cpp \
    model/devicetiming.cpp \
    model/message.cpp \
    model/scenetiming.cpp \
    model/devicelist.cpp

HEADERS += \
    qtgpio.h \
    mqttclient.h \
    mainwindow.h \
    database/dataquery.h \
    mqtt/qmqtt_client_p.h \
    mqtt/qmqtt_client.h \
    mqtt/qmqtt_frame.h \
    mqtt/qmqtt_global.h \
    mqtt/qmqtt_message_p.h \
    mqtt/qmqtt_message.h \
    mqtt/qmqtt_network_p.h \
    mqtt/qmqtt_networkinterface.h \
    mqtt/qmqtt_routedmessage.h \
    mqtt/qmqtt_router.h \
    mqtt/qmqtt_routesubscription.h \
    mqtt/qmqtt_socket_p.h \
    mqtt/qmqtt_socketinterface.h \
    mqtt/qmqtt_ssl_socket_p.h \
    mqtt/qmqtt_timer_p.h \
    mqtt/qmqtt_timerinterface.h \
    mqtt/qmqtt_websocket_p.h \
    mqtt/qmqtt_websocketiodevice_p.h \
    mqtt/qmqtt.h \
    net/netclient.h \
    net/tcpservice.h \
    net/udpclient.h \
    net/userclient.h \
    serialport/serialdata.h \
    wpamsg.h \
    userdatarequest.h \
    stringquery.h \
    signalbar.h \
    scanresultsitem.h \
    scanresults.h \
    peers.h \
    networkconfig.h \
    gwwpagui.h \
    eventhistory.h \
    addinterface.h \
    frminput.h \
    model/gwinfo.h \
    model/deviceinfo.h \
    model/devicetype.h \
    model/houseinfo.h \
    model/sceneinfo.h \
    model/basedevice.h \
    model/cmdinfo.h \
    serialport/modbusclient.h \
    model/taskinfo.h \
    model/condition.h \
    model/conditiontask.h \
    model/timing.h \
    model/controller.h \
    model/devicetiming.h \
    model/message.h \
    model/scenetiming.h \
    model/devicelist.h

FORMS += \
    mainwindow.ui \
    userdatarequest.ui \
    scanresults.ui \
    peers.ui \
    networkconfig.ui \
    gwwpagui.ui \
    eventhistory.ui \
    frminput.ui

DISTFILES += \
    smarthome.sql \
    smarthome.sqlite \
    wpa/wpa_gui.desktop \
    wpa_gui.desktop

DEFINES += CONFIG_CTRL_IFACE_UNIX
INCLUDEPATH	+= mqtt \
    . .. ./src ./src/utils
RESOURCES += \
    res.qrc
SUBDIRS += \
    mqtt/qmqtt.pro
